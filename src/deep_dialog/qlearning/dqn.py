# tensorflow network #
import tensorflow as tf
import numpy as np
import random
# Parameters
MAX_EPSILON = 1
MIN_EPSILON = 0.01
LAMBDA = 0.0001
GAMMA = 0.99
BATCH_SIZE = 16
learning_rate = 0.001

class DQN:
    def __init__(self, input_size, hidden_size, output_size):
                
        tf.reset_default_graph()
        
        self.input_size = input_size        
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.gamma = GAMMA
        self.lr = learning_rate
        self.learn_step_counter = 0
        self.replace_target_iter = BATCH_SIZE

        self.s = tf.placeholder(tf.float32, [None, self.input_size], name='state')  # input State
        self.s_ = tf.placeholder(tf.float32, [None, self.input_size], name='s_next')  # input Next State
        self.r = tf.placeholder(tf.float32, [None,1], name='r')  # input Reward
        self.a = tf.placeholder(tf.int32, [None,1], name='a')  # input Action
        self.qtarget = tf.placeholder(tf.float32,[None, self.output_size], name='qtarget')
        

        self.build_prediction_net()
        #self.build_target_net()
        self.build_trainning()

        # p_params = tf.get_collection('primary_network_parameters')
        # t_params = tf.get_collection('target_network_parameters')
        # self.z = tf.identity(p_params[0],name='p_params')

        # with tf.variable_scope('hard_replacement'):
        #     self.replacing_target_parameters = [tf.assign(t, p) for t, p in zip(t_params, p_params)]
        
       
        self.sess = tf.Session()
        #init_op = tf.global_variables_initializer()
        

        self.sess.run(tf.global_variables_initializer())
        # Add ops to save and restore all the variables.
        self.saver = tf.train.Saver()

        #self.saver.restore(sess, tf.train.latest_checkpoint(path))



        
    

    def build_prediction_net(self):
        # ------------------ build evaluate_net ------------------


        with tf.variable_scope('pred_network'):  

            c = ['primary_network_parameters', tf.GraphKeys.GLOBAL_VARIABLES]     


            # first layer
            with tf.variable_scope('layer1'):
                w1 = tf.get_variable('w1', [self.input_size, self.hidden_size],
                                     dtype=tf.float32,collections=c)
                b1 = tf.get_variable('b1', [1, self.hidden_size],
                                     dtype=tf.float32,collections=c)
                l1 = tf.nn.tanh(tf.matmul(self.s, w1) + b1)

            # second layer
            with tf.variable_scope('layer2'):
                w2 = tf.get_variable('w2', [self.hidden_size, self.output_size],
                                     dtype=tf.float32,collections=c)
                b2 = tf.get_variable('b2', [1, self.output_size],
                                     dtype=tf.float32,collections=c)
                self.q_eval = tf.matmul(l1, w2) + b2
                self.q_action = tf.argmax(self.q_eval, axis = 1)          

            

    def build_target_net(self):
        # ------------------ build  target_net ------------------

        
        with tf.variable_scope('target_network'): 

            c = ['target_network_parameters', tf.GraphKeys.GLOBAL_VARIABLES]


            
                        # first layer
            with tf.variable_scope('layer1'):
                w1 = tf.get_variable('w1', [self.input_size, self.hidden_size],
                                     dtype=tf.float32,collections=c)
                b1 = tf.get_variable('b1', [1, self.hidden_size],
                                     dtype=tf.float32,collections=c)
                l1 = tf.nn.tanh(tf.matmul(self.s_, w1) + b1)

            # second layer
            with tf.variable_scope('layer2'):
                w2 = tf.get_variable('w2', [self.hidden_size, self.output_size],
                                     dtype=tf.float32,collections=c)
                b2 = tf.get_variable('b2', [1, self.output_size],
                                     dtype=tf.float32,collections=c)

                self.target_q = tf.matmul(l1, w2) + b2
                self.target_q_action = tf.argmax(self.target_q, axis=-1)


    def build_trainning(self):
 

        with tf.variable_scope('loss'):            
            self.loss = tf.losses.mean_squared_error(self.qtarget, self.q_eval)
        with tf.variable_scope('train'):
            #self.optimizer = tf.train.AdamOptimizer(0.001).minimize(self.loss)
            self.optimizer = tf.train.RMSPropOptimizer(0.001).minimize(self.loss)
 






    def run_tf_policy(self, representation, num_actions, experience_replay_pool, experience_replay_pool_size, rule_policy, warm_start):
     
        epsilon = 0 
        if random.random() < epsilon:
            return random.randint(0, num_actions - 1)
        else:
            if warm_start == 1:
                if len(experience_replay_pool) > experience_replay_pool_size:
                    warm_start = 2
                return rule_policy()
            else:
                #print(representation.shape) (1,272)
                q_action = self.sess.run(self.q_action, feed_dict={self.s : representation})
                #print(q_action)
                return q_action[0]
    
    def tensor_train(self, batch_size, num_batches, experience_replay_pool, sample_from_buffer):
        """ Train DQN with experience replay """
        

        #self.running_expereince_pool = self.experience_replay_pool

        for i in range(1):
            for iter_batch in range(num_batches):
                self.cur_bellman_err = 0
                for iter in range(len(experience_replay_pool)/(batch_size)):

                    batch = sample_from_buffer(batch_size)
                    #print(batch.action.shape)
                    target_q, target_q_action, q_eval = self.sess.run([self.target_q, self.target_q_action, self.q_eval], \
                        feed_dict={ 
                            self.s: batch.state,
                            self.s_: batch.next_state,
                            self.a : batch.action
                    })
                    


                    q_target = q_eval.copy()

                    batch_index = np.arange(batch_size, dtype=np.int32)
                    eval_act_index = batch.action.astype(int)
                    reward = batch.reward

                    q_target[batch_index, eval_act_index] = reward + self.gamma * np.max(target_q , axis=1)            

                    _, cost = self.sess.run(    \
                        [self.optimizer, self.loss], feed_dict = { \
                            self.s: batch.state,
                            self.a : batch.action,
                            self.qtarget : q_target
                        })

                    #print("cost", cost)
                    self.cur_bellman_err += cost              
    
